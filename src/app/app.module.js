(function () {
  'use strict';

  angular.module('app', [
    'pascalprecht.translate',
    'tmh.dynamicLocale',
    'ui.router',
    'ngAnimate',
    'ngTouch',
    // 'ngSanitize',
    'ui.bootstrap',
    'ngMeta'
  ])

})();
