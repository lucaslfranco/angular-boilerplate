(function () {
  'use strict';

  angular.module('app').component('california', {
    controller: CaliforniaController,
    controllerAs: 'vm',
    templateUrl: 'app/california/california.view.html',
  })
    .directive('fancybox', function ($compile) {
      return {
        restrict: 'A',
        replace: false,
        link: function ($scope, element) {

          $scope.open_fancybox = function () {
            var el = angular.element(element.html()),
              compiled = $compile(el);
            $.fancybox.open(el);
            compiled($scope);

          }
        }
      }
    })
    .directive("scroll", function ($window) {
      return function (scope, element, attrs) {
        angular.element($window).bind("scroll", function () {
          var homeHeight = document.getElementById('home').offsetHeight / 2;
          var finalFormScrollPosition =
            document.getElementById('formulario-final').getBoundingClientRect().y - document.getElementById('formulario-final').offsetHeight;

          scope.showVisitIcon = (this.pageYOffset > homeHeight && finalFormScrollPosition > 0);
          if (this.pageYOffset < homeHeight || finalFormScrollPosition < 0) {
            scope.showFloatingForm = false;
          }
          // scope.showFloatingForm = finalFormScrollPosition > 0);
          scope.$apply();
        })
      }
    })

  /** @ngInject */
  function CaliforniaController($log, $scope, $state, $http, $document, $location) {

    var vm = $scope;

    var url = 'http://lyxengenharia.arboimoveis.com/api/cards';
    //var url = 'http://arbo-admin.letscomunica.com.br/api/cards';
    //var url = 'http://192.168.0.112:3000/api/cards';

    var empreendimento = 'California';
    var emp_id = 2;

    var imagesUrl = '/assets/images';

    vm.californiaData = {
      name: 'Califórnia',
      route: '/california',
      logo: imagesUrl + '/logo-california.png',
      conheca: {
        url: 'http://lyxengenharia.arboimoveis.com/madison',
        image: imagesUrl + '/logo-madison.png'
      },
      values: {
        entrada: '1.000,00',
        parcela: '299,00',
        subsidio: '37.000,00'
      },
      address: 'Rua Maria Joaquina Vaz, s/n, Campo do Meio - Campo Largo',
      construction_company: 'Lyx Engenharia e Projeto Residencial X11 SPE LTDA',
      archtecture_project: 'V Arquitetura LTDA ME',
      area: '137.040,97 m²',
      area_building: '137.040,38 m²',
      location: { lat: -25.475179, lng: -49.577126 },
      recreations: [
        '03 Piscinas',
        '02 Quadras Poliesportivas',
        '02 Playgrounds',
        '03 Salões de Festas Gourmet',
        '02 Salas de Jogos',
        '02 Academias',
        '02 Salas de Cinema',
        '05 Churrasqueiras',
        '02 Bicicletários',
        '03 Pet Play',
        '03 Redários',
        '01 Gaiolas Driving Range',
        '03 Quioesques duplos com churrasqueiras',
        '02 Garage Band',
        'Pista de Corrida/Caminhada',
        'Praça de Convivência',
        'Solário',
        'Espaço Zen',
        'Sacada com churrasqueira'
      ],
      mainCarousel: [
        {
          id: 0,
          image: imagesUrl + '/california/slider-california-01.png'
        },
        {
          id: 1,
          image: imagesUrl + '/california/slider-california-02.png'
        }
      ],
      gallery: [
        {
          image: imagesUrl + '/california/img-slider-california01.jpg',
          largeImage: imagesUrl + '/california/img-slider-california01.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california02.jpg',
          largeImage: imagesUrl + '/california/img-slider-california02.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california03.jpg',
          largeImage: imagesUrl + '/california/img-slider-california03.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california04.jpg',
          largeImage: imagesUrl + '/california/img-slider-california04.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california05.jpg',
          largeImage: imagesUrl + '/california/img-slider-california05.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california06.jpg',
          largeImage: imagesUrl + '/california/img-slider-california06.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california07.jpg',
          largeImage: imagesUrl + '/california/img-slider-california07.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california08.jpg',
          largeImage: imagesUrl + '/california/img-slider-california08.jpg'
        },
        {
          image: imagesUrl + '/california/img-slider-california09.jpg',
          largeImage: imagesUrl + '/california/img-slider-california09.jpg'
        }
      ],
      plants: [
        {
          image: imagesUrl + '/california/planta-01.png',
          size: '40,00',
          features: [
            '02 Quartos',
            'Banheiro',
            'Sala de Estar',
            'Sala de Jantar',
            'Cozinha',
            'Lavanderia'
          ]
        },
        {
          image: imagesUrl + '/california/planta-02.png',
          size: '43,50',
          features: [
            '02 Quartos',
            'Banheiro',
            'Sala de Estar',
            'Sala de Jantar',
            'Cozinha',
            'Churrasqueira'
          ]
        }
      ],
      technical_image: imagesUrl + '/california/img-fichatecnica.png',
      legal_text: 'O projeto encontra-se aprovado na prefeitura Municipal de Campo Largo/PR sob alvará nº 247/2016. As espécies que compõem o paisagismo serão entregues em idade jovem, podendo seus tamanhos variar em relação às imagens. Projeto Arquitetônico: FV Arquitetura Ltda Me, CAU 32833-2. Engenheiro responsável: Carlos Rubiano Martins, CREA PR-86053/D. *Condicionada à aprovação cadastral com enquadramento pela CEF. * Em caso de parcelamento durante a fase de obras os valores das parcelas serão corrigidos pelo INCC-M/FGV.” * Imagens meramente ilustrativas. Móveis e decoração não fazem parte da unidade. As informações acima exibidas, poderão sofrer mudanças sem aviso prévio.'
    }

    vm.showVisitForm = true;
    vm.showDateForm = false;
    vm.showVisitIcon = false;
    vm.showFloatingForm = false;

    vm.formData = {
      card_nome: '',
      card_telefone: '',
      card_email: '',
      card_interesse: 'Venda',
      card_plantao: 'Plantão Norte',
      empreendimento_nome: empreendimento,
      status_id: 1
    };

    var req = {
      method: 'POST',
      url: url,
      headers: {
        'Content-Type': 'application/json'
      },
      data: vm.formData
    }

    // Formulários
    vm.submitVisitForm = function () {
      $http(req).then(function (response) {
        $log.log(response)

        vm.formData.id = response.data.id;
        vm.formData.status_id = 2;

        vm.showVisitForm = false;
        vm.showDateForm = true;
      });
    }

    vm.submitDateForm = function () {

      fixDate();

      var req = {
        method: 'PUT',
        url: url,
        headers: {
          'Content-Type': 'application/json'
        },
        data: vm.formData
      }

      $http(req).then(function (response) {
        vm.changePath('/obrigado')
        $log.log(response)
        callSuccessAlert();
      });
    }

    vm.submitFinalForm = function () {

      fixDate();

      $http(req).then(function (response) {
        vm.changePath('/obrigado');
        $log.log(response)
        callSuccessAlert();
      });
    }

    vm.showMobileForm = function () {
      vm.showFloatingForm = !vm.showFloatingForm;
      vm.showVisitIcon = false;

      $document[0].body.classList.toggle('no-scroll');
    }

    vm.closeForm = function () {
      vm.showFloatingForm = false;
      $document[0].body.classList.toggle('no-scroll');
    }

    vm.changePath = function (path) {
      console.log('Path changed to ' + vm.californiaData.route + path);
      $state.transitionTo('/obrigado');
      // $location.path(vm.californiaData.route + '/obrigado');
    }

    function callSuccessAlert() {
      swal({
        title: "Obrigado!",
        text: "Sua visita foi agendada com sucesso.",
        icon: "success",
        button: "Fechar",
      })
        .then(function () {
          vm.changePath('/');
          vm.showFloatingForm = false;
        })
    }

    function fixDate() {
      var data = vm.formData.card_visita_data;
      if (data.replace) {
        // input radio retorna data com ""
        data = data.replace('"', '').replace('"', '');
        vm.formData.card_visita_data = data;
      }
    }

    // Imagens
    vm.mainCarouselInterval = 5000;

    // Datepicker e Timepicker
    vm.datesPage = 0;
    vm.timesPage = 0;

    var startDate = new Date();
    vm.dates = getDates(startDate, 30);
    vm.formData.card_visita_data = vm.dates[1].date;
    vm.tempDates = [vm.dates[0], vm.dates[1], vm.dates[2]];

    vm.times = getTimes();
    vm.formData.card_visita_hora = vm.times[0].time;
    vm.tempTimes = [vm.times[0], vm.times[1], vm.times[2]];


    function dayAsString(dayIndex) {
      var weekdays = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB'];
      return weekdays[dayIndex];
    }

    function getDates(startDate, daysToAdd) {
      var dates = [];
      for (var i = 0; i < daysToAdd; i++) {
        var currentDate = new Date();
        currentDate.setDate(startDate.getDate() + i + 1);
        dates.push({ date: new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()), dayOfWeek: dayAsString(currentDate.getDay()), day: ("0" + currentDate.getDate()).slice(-2), month: ("0" + (currentDate.getMonth() + 1)).slice(-2) });
      }
      return dates;
    }

    vm.$watch('datesPage', function (newValue, oldValue) {
      if (newValue < 0) vm.datesPage = 0;
      else if (newValue > (vm.dates.length / 3) - 1) vm.datesPage = oldValue;

      if (newValue !== oldValue) {
        vm.tempDates = [vm.dates[vm.datesPage * 3], vm.dates[vm.datesPage * 3 + 1], vm.dates[vm.datesPage * 3 + 2]];
        vm.formData.card_visita_data = vm.tempDates[0].date;
      }
    })

    function getTimes() {
      var times = [
        { time: '09:00' }, { time: '09:15' }, { time: '09:30' }, { time: '09:45' },
        { time: '10:00' }, { time: '10:15' }, { time: '10:30' }, { time: '10:45' },
        { time: '11:00' }, { time: '11:15' }, { time: '11:30' }, { time: '11:45' },
        { time: '12:00' }, { time: '12:15' }, { time: '12:30' }, { time: '12:45' },
        { time: '13:00' }, { time: '13:15' }, { time: '13:30' }, { time: '13:45' },
        { time: '14:00' }, { time: '14:15' }, { time: '14:30' }, { time: '14:45' },
        { time: '15:00' }, { time: '15:15' }, { time: '15:30' }, { time: '15:45' },
        { time: '16:00' }, { time: '16:15' }, { time: '16:30' }, { time: '16:45' },
        { time: '17:00' }, { time: '17:15' }, { time: '17:30' }, { time: '17:45' },
      ];
      return times;
    }

    vm.$watch('timesPage', function (newValue, oldValue) {
      if (newValue < 0) vm.timesPage = 0;
      else if (newValue > (vm.times.length / 3) - 1) vm.timesPage = oldValue;

      if (newValue !== oldValue) {
        vm.tempTimes = [vm.times[vm.timesPage * 3], vm.times[vm.timesPage * 3 + 1], vm.times[vm.timesPage * 3 + 2]];
        vm.formData.card_visita_hora = vm.tempTimes[0].time;
      }
    })

    vm.setCheckedTime = function (time) {
      vm.times.forEach(element => {
        element.checked = false;
      })
      time.checked = true;
    }

    // Inicializa o mapa
    function initMap() {
      var local = vm.californiaData.location;
      var map = new google.maps.Map(
        $document[0].getElementById('map'), { zoom: 16, center: local });
      new google.maps.Marker({ position: local, map: map });
    }

    // Suaviza as movimentações pelas "ancoras" (home, california, plantas, localização)
    $document[0].querySelectorAll('a[href^="#"]').forEach(anchor => {
      anchor.addEventListener('click', function (e) {
        e.preventDefault();

        $document[0].querySelector(this.getAttribute('href')).scrollIntoView({
          behavior: 'smooth', block: 'start'
        })
      })
    })

    initMap();
  }
})();
