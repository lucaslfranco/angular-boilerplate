(function () {
  'use strict';

  angular.module('app').config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, ngMetaProvider) {
    $stateProvider.state('home', {
      url: '/',
      component: 'madison',
      data: {
        meta: {
          'title': 'Madison - Condomínio Clube | Lyx Engenharia',
          'description': 'More em um condomínio clube com apenas R$ 1.000,00 de entrada! Inspirado nos condomínios do Sul da Califórnia, este novo estilo de vida oferece o conforto para quem gosta de morar em casas, com toda a privacidade, facilidade e comodidade de quem gosta de morar em um apartamento. (41) 3907-5345',
          'keywords': 'Lyx, Riverside, Condomínio, Prédio, imobiliárias, Campo do Meio, Campo Largo, Araucária, Curitiba, Condomínio, Casas, Casa, Sobreposta, Imóvel, Imóveis, Comprar, Apartamento, Venda, Comprar, prédio, prédios, Entrada, Facilitada, Crédito, Caixa, Financiamento, Minha Casa Minha Vida, Clube, Lazer, Engenharia, Construtora, planta',
          'og:url': 'http://lyxengenharia.arboimoveis.com/madison',
          'image': 'http://lyxengenharia.arboimoveis.com/madison/assets/images/logo-madison.png',
          'appId': '193435588178321'

        }
      }
    });
    $stateProvider.state('madison', {
      url: '/madison',
      component: 'madison',
      data: {
        meta: {
          'title': 'Madison - Condomínio Clube | Lyx Engenharia',
          'description': 'More em um condomínio clube com apenas R$ 1.000,00 de entrada! Inspirado nos condomínios do Sul da Califórnia, este novo estilo de vida oferece o conforto para quem gosta de morar em casas, com toda a privacidade, facilidade e comodidade de quem gosta de morar em um apartamento. (41) 3907-5345',
          'keywords': 'Lyx, Riverside, Condomínio, Prédio, imobiliárias, Campo do Meio, Campo Largo, Araucária, Curitiba, Condomínio, Casas, Casa, Sobreposta, Imóvel, Imóveis, Comprar, Apartamento, Venda, Comprar, prédio, prédios, Entrada, Facilitada, Crédito, Caixa, Financiamento, Minha Casa Minha Vida, Clube, Lazer, Engenharia, Construtora, planta',
          'og:url': 'http://lyxengenharia.arboimoveis.com/madison',
          'image': 'http://lyxengenharia.arboimoveis.com/madison/assets/images/logo-madison.png',
          'appId': '193435588178321'

        }
      }
    });
    $stateProvider.state('california', {
      url: '/california',
      component: 'california',
      data: {
        meta: {
          'title': 'California - Condomínio Clube | Lyx Engenharia',
          'description': 'More em um condomínio clube com apenas R$ 1.000,00 de entrada! Inspirado nos condomínios do Sul da Madisonfórnia, este novo estilo de vida oferece o conforto para quem gosta de morar em casas, com toda a privacidade, facilidade e comodidade de quem gosta de morar em um apartamento. (41) 3907-5345',
          'keywords': 'Lyx, Riverside, Condomínio, Prédio, imobiliárias, Campo do Meio, Campo Largo, Araucária, Curitiba, Condomínio, Casas, Casa, Sobreposta, Imóvel, Imóveis, Comprar, Apartamento, Venda, Comprar, prédio, prédios, Entrada, Facilitada, Crédito, Caixa, Financiamento, Minha Casa Minha Vida, Clube, Lazer, Engenharia, Construtora, planta',
          'og:url': 'http://lyxengenharia.arboimoveis.com/madison',
          'image': 'http://lyxengenharia.arboimoveis.com/madison/assets/images/logo-madison.png',
          'appId': '193435588178321'
        }
      }
    });

    $urlRouterProvider.otherwise('/');
  }

})();
